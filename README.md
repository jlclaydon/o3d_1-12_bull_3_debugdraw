# o3d_1-12_bull_3_debugdraw

Extended 2nd base project for CMU students studying Game Engine Development.

This one combines Ogre3d (1.12) and Bullet (3.0) and makes use of [btOgre](https://github.com/OGRECave/btogre?files=1) a bullet/ogre interface which provides loads of useful functionality.  Here I'm using it to extract graphical information from bullet on the collision volumes in the world which are passed to ogre for rendering.  It does a load of other cool stuff and is well worth a look.

## Install Note ##

The install is simple (as in this project, ) we just copy in the required files and add them to the build. 